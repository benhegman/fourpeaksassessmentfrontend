import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import mapboxgl from "mapbox-gl";
import Timeline from "./features/timeline/Timeline";
import axios from "axios";
import { Button } from "@material-ui/core";
import TimelineRoundedIcon from "@material-ui/icons/Timeline";
import * as turf from "@turf/turf";
// always import with the * unless you wanna spend an hour with a "broken" turf package

mapboxgl.accessToken = process.env.REACT_APP_ACCESS_TOKEN;

// base django rest api url
const databaseUrl = process.env.REACT_APP_DATABASE;

class Application extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      lng: -119.2748,
      lat: 45.9357,
      zoom: 13.75,
      showPathOn: false,
      selectedFish: null,
      fishDates: [],
      hoursDiff: null,
      sliderValue: 0,
      fishPoint: {
        type: "FeatureCollection",
        features: [
          {
            type: "Feature",
            properties: {},
            geometry: {
              type: "Point",
              coordinates: [],
            },
          },
        ],
      },
      lineShowing: null,
      map: {},
    };
  }

  // call back function for when a new fish is selected - it sets dates and path state
  updatePath = () => {
    const line = JSON.parse(
      this.state.selectedFish.time_coords.replace(/'/g, '"')
    );
    const fishDates = [line[0].date, line[line.length - 1].date]; // line is format {0:{date:<date> coords: [coord[0], coord[1]]}, 1: ...}
    this.setState({ fishDates: fishDates });
    const cleanedLine = line.map((dict) => {
      const coords = dict.coords;
      return [...coords];
    });
    // turned the line into a turf Linestring and then interpolated it with a Bezier spline algorithm
    const turfedLine = turf.lineString(cleanedLine);
    const bezier = turf.bezierSpline(turfedLine);
    this.setState(
      {
        fishPoint: turf.point(bezier.geometry.coordinates[0]),
        timeCoords: line,
        lineShowing: bezier,
      },
      () => {
        // this part updates the point and line that are showing once they have been set to state
        this.state.map.getSource("fishPoint").setData(this.state.fishPoint);
        this.state.map.getSource("fishPath").setData(this.state.lineShowing);
      }
    );
  };

  // called when the show path button is clicked - changes mapbox layer properties
  togglePathVisibility = () => {
    this.state.showPathOn
      ? this.state.map.setLayoutProperty("fishyPath", "visibility", "visible")
      : this.state.map.setLayoutProperty("fishyPath", "visibility", "none");
  };

  // takes the click event as input when fish is clicked
  addPopupContent = (fishyClick) => {
    const popupContainer = document.createElement("div");

    const popupContent = (
      <div>
        <div>
          {" "}
          {/* maps through the fish's properties and only displays those that exist in the popup */}
          {Object.entries(fishyClick.properties).map((property, key) => {
            if (
              property[0] !== "time_coords" &&
              !["nan", "NaT"].includes(property[1])
            ) {
              return (
                <div key={key}>
                  <span>
                    <strong>{property[0]}</strong>:{property[1]}
                  </span>
                </div>
              );
            }
            return null;
          })}
        </div>
        <div className="text-center">
          <Button
            className="show-path-btn"
            onClick={() =>
              this.setState(
                (prevState) => ({
                  ...prevState,
                  showPathOn: !prevState.showPathOn,
                }),
                () => {
                  this.togglePathVisibility(); // show and hide fish path
                }
              )
            }
          >
            <TimelineRoundedIcon fontSize="large" />
          </Button>
          <p className="text-center">
            {this.state.showPathOn ? "Hide Fish Path" : "Show Fish Path"}{" "}
            {/* needs updating so that it updates wihtout having to close and reopen a popup */}
          </p>
        </div>
      </div>
    );
    ReactDOM.render(popupContent, popupContainer);
    return popupContainer;
  };

  // once container is mounted
  componentDidMount() {
    const map = new mapboxgl.Map({
      container: this.mapContainer,
      style: "mapbox://styles/mapbox/streets-v11",
      center: [this.state.lng, this.state.lat],
      zoom: this.state.zoom,
    });

    map.on("load", async () => {
      // request data from the api
      let res = await axios.get(databaseUrl);
      const features = {
        type: "FeatureCollection",
        features: res.data.map((fish) => {
          return {
            type: "Feature",
            properties: { ...fish },
            geometry: {
              type: "Point",
              coordinates: JSON.parse(fish.time_coords.replace(/'/g, '"'))[0] // create a array of dicts (coords) from string
                .coords,
            },
          };
        }),
      };

      // source and layer for the fishes path LineString
      map.addSource("fishPath", {
        type: "geojson",
        data: this.state.lineShowing,
      });
      map.addLayer({
        id: "fishyPath",
        type: "line",
        source: "fishPath",
        layout: {
          "line-join": "round",
          "line-cap": "round",
          visibility: "none",
        },
        paint: {
          "line-color": "#888",
          "line-width": 2,
        },
      });
      // source and layer for the Point along the path
      map.addSource("fishPoint", {
        type: "geojson",
        data: this.state.fishPoint,
      });

      map.addLayer({
        id: "point",
        source: "fishPoint",
        type: "symbol",
        layout: {
          "icon-image": "aquarium-15",
          "icon-allow-overlap": true,
          "icon-ignore-placement": true,
        },
      });

      // source and layer for all the visible fish
      map.addSource("fish", {
        type: "geojson",
        data: features,
      });
      map.addLayer({
        id: "fishies",
        source: "fish",
        type: "circle",
        paint: {
          "circle-color": [
            "match",
            ["get", "species_name"],
            "Coho",
            "#fbb03b",
            "Steelhead",
            "#5B7F64",
            "Chinook",
            "#e55e5e",
            "nan",
            "#3bb2d0",
            /* other */ "#000000",
          ],
        },
      });
    });
    this.setState({ map: map });

    // handled when a fish on the screen is clicked - adds popup and sets the fish details/dates and path
    map.on("click", "fishies", (e) => {
      const coordinates = e.features[0].geometry.coordinates.slice();
      const fishClick = e.features[0];
      // next lines are so if zoomed all the way out the popup only occurs once
      while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
        coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
      }
      new mapboxgl.Popup()
        .setDOMContent(this.addPopupContent(fishClick))
        .setLngLat(coordinates)
        .addTo(map);

      this.setState(
        { selectedFish: e.features[0].properties },
        this.updatePath
      );
    });

    map.on("move", () => {
      this.setState({
        lng: map.getCenter().lng.toFixed(4),
        lat: map.getCenter().lat.toFixed(4),
        zoom: map.getZoom().toFixed(2),
      });
    });
  }

  render() {
    return (
      <div>
        <div className="sidebarStyle">
          <div>
            Longitude: {this.state.lng} | Latitude: {this.state.lat}
          </div>
        </div>

        <div ref={(el) => (this.mapContainer = el)} className="mapContainer" />
        <Timeline
          lineShowing={this.state.lineShowing}
          fishDetails={this.state.selectedFish}
          fishDates={this.state.fishDates}
          map={this.state.map}
        />

        <div className="legend">
          <div className="legend-title">Species</div>
          <div className="legend-scale">
            <ul className="legend-labels">
              <li>
                <span style={{ background: "#fbb03b" }}></span>Coho
              </li>
              <li>
                <span style={{ background: "#e55e5e" }}></span>Chinook
              </li>
              <li>
                <span style={{ background: "#5B7F64" }}></span>Steelhead
              </li>
              <li>
                <span style={{ background: "#3bb2d0" }}></span>Unknown
              </li>
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

ReactDOM.render(<Application />, document.getElementById("app"));
